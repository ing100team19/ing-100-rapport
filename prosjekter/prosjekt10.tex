%!TEX root = ../main.tex
\chapter[Prosjekt 10: Sykkelcomputer]{Prosjekt 10: Sykkelcomputer {\large (Kristoffer \& Stian)}}
\label{kap:sykkel-odometer}

\section{Problemstilling}
I dette prosjektet ble det bygget en sykkelkomputer (figur~\ref{fig:lego:sykkel}) som ved hjelp av lys- og ultralyd-sensor kunne beregne distanse, hastighet og akselerasjon til et sykkelhjul.

\begin{figure}[H]
\centering
\includegraphics[width = 0.8\textwidth]{images/project10/lego_structure.jpg}
\caption{Legostrukturen som ble bygget i dette prosjektet. Strukturen består av to sensorer, og en motor som roterer en legopinne. Legopinnen må være bred nok til at ultralyd-sensoren registrerer passeringer, og ha stor nok kontrast med bakgrunnen slik at lys-sensoren også registrerer passeringer.}
\label{fig:lego:sykkel}
\end{figure}

\section{Forslag til løsning}
Først ble radiusen til legopinnen målt til, denne radiusen ble brukt til å beregne omkretsen av hjulet, som var lik \SI{53.09}{cm}. 

Motoravlesningene ble benyttet til å kalkulere kontrollverdier for distanse, hastighet og akselerasjon, slik at resultatene fra sykkelkomputeren kan verifiseres. Nedenfor er et utdrag fra koden for hvordan dette ble gjort, hentet ifra \verb|P10_F4_MathCalculation.m|. 

\begin{codesample}
PosMmA(k)=(PosMotorA(k)/360*845); 
DistanseFiltrert(k)=(IIR_filter(PosMmAFilt(k-1),PosMmA(k),alpha))/1000;

HastighetA(k)=Derivation(PosMmAFilt(k-1:k),Ts(k-1));
AkslerasjonA(k)=Derivation(FartA(k-1:k),Ts(k-1));
\end{codesample}

Her ble posisjonen til motoren omgjort til millimeter, og tilbakelagt distanse ble kalkulert og omgjort til meter. Hastighet og akselerasjon ble også kalkulert basert på motoravlesninger og et tidsintervall, $Ts$.

For å bestemme om sykkelhjulet beveget seg forover eller bakover kun ved hjelp av lys- og ultralyd-sensoren ble plottet for sensorene studert. Plottet~(se figur~\ref{fig:USandL}) viste at dersom lys-sensoren registrer et utslag rett før ultralyd-sensoren betyr dette at sykkelhjulet beveger seg forover, og vice versa for bakover rotasjon. 


\begin{figure}[H]
\centering
\includegraphics[width = 0.9\textwidth]{images/project10/UltrasonicandLight.png}
\caption{Målte verdier fra lys- og ultralyd-sensoren. 
Fra starten av plottet observeres utslag fra lys-sensoren foran utslag fra ultralyd-sensoren, noe som betyr positiv bevegelse. Bevegelsen stanser etter 8 sekund, og starter igjen noen sekund senere, nå som en negativ bevegelse, da ultralyd-sensoren har utslag foran lys-sensoren. Økende rotasjonshastighet fører til hyppigere frekvens, som observeres mot slutten av plottet.}
\label{fig:USandL}
\end{figure}

Kodeutdraget under viser koden for ultralyd-sensoren, tilsvarende kode er også implementert for lys-sensoren. For at et bunnpunkt skal registreres må fire if-betingelser oppfylles:
\begin{itemize}
\item Måling $k-2$ må være større enn måling $k-1$
\item Måling $k$ må være større enn måling $k-1$
\item Måling $k-1$ må være lavere enn en grenseverdi, her benyttes 20 for ultralyd-sensoren
\item Tiden for målingen må være minst $0.2$ sekunder etter forrige bunnpunktsmåling 
\end{itemize}
Dersom alle fire kriteriene blir oppfylt blir den målte tiden registrert og lagret til en vektor, og antall runder øker med 1.
\begin{codesample}
if UltraSonic(k-1) < UltraSonic(k-2) && UltraSonic(k-1) < UltraSonic(k)
	&& UltraSonic(k-1) < 20 && (Tid(k) - UStid) > 0.2
    UStid = Tid(k);				UStidV = [UStidV UStid];
    USrunde = USrunde + 1; 		USrundeV = [USrundeV USrunde]; 
end
\end{codesample}


Under er koden som ble brukt til å bestemme retningen av rotasjonen. På samme vis som for bunnpunktsregistrering, ble fire if-betingelser benyttet:
\begin{itemize}
\item Lys-sensoren må ha registrert minst en passering
\item Ultralyd-sensoren må ha registrert minst en passering
\item Dersom tiden til målingen fra ultralyd-sensoren er større enn for lys-sensoren blir det registrert positiv rotasjon, og motsatt for negativ rotasjon
\item Tiden for målingen må være minst 0.3 sekunder etter forrige rundemåling 
\end{itemize}

\begin{codesample}
if Lrunde > 0 && USrunde > 0 && UStidV(USrunde) - LtidV(Lrunde) > 0 && 
	(UStidV(USrunde) - LtidV(Lrunde)) <  0.3  
    PosRunde(USrunde) = 1.1624;     
    
elseif USrunde > 0 && Lrunde > 0 && UStidV(USrunde) - LtidV(Lrunde) < 0 
	&& (LtidV(Lrunde) - UStidV(USrunde)) < 0.3   
    PosRunde(USrunde) = -1.1624;
end
\end{codesample}

\section{Resultat}
Under i figur~\ref{fig:result:proj10} vises resultatet fra sensorene sammenlignet med kontrollverdiene fra motoren. I øverste plott(a), som viser posisjon, har en høy korrelasjon mellom kontrollverdier og sensor-verdiene. Her er også individuelle sensormålinger fra lys- og ultralyd-sensoren plottet,  disse er kun plottet basert på tidsverdien og ikke selve målingsverdien. Dette er for å illustrere at retningen av rotasjonen bestemmes ved hjelp av rekkefølgen til sensormålingene.

I plott(b) og plott(c) hvor hastighet og akselerasjon er plottet er det mer avvik. Dette skyldes i hovedsak måten hastighet og akselerasjon blir kalkulert ved bruk av motormålingene. Små unøyaktigheter i målingene vil gi store utslag når de blir derivert over et lite tidsintervall. Et eksempel på dette er når et instrument har en måleusikkerhet på $0.01$, og målingene registreres hvert $\SI{0.001}{s}$, dette vil gi et utslag på resultatet lik: $\frac{2\cdot \text{måleusikkerhet}}{\text{tidsintervall}} \rightarrow \frac{0.02}{0.001} = 20$. For akselerasjonen blir denne operasjonen gjort to ganger, noe som fører til en eksponentiell øking i usikkerheten til enkeltmålinger. 

\begin{figure}[hbt]
\centering
{\large \includesvg[width=\textwidth]{images/project10/odometer}}
\caption{Plottene viser en sammenligning mellom verdier basert på diskrete målinger fra sensorene og kontinuerlige målinger ifra motoren. 
Sammenligningen av posisjonsmålinger(a) viser god korrelasjon mellom de to datasettene. Individuelle sensormålinger er også plottet,  disse er kun plottet for tidsverdien. I sammenligningen av hastighet(b) og akselerasjon(c) er det noe mer avvik. Dette skyldes at solide målinger krever mer kontinuerlige målingspunkter. Motoren har omtrent 1200 målinger, sammenlignet med sensorene som har 18 rundemålinger.}
\label{fig:result:proj10}
\end{figure}