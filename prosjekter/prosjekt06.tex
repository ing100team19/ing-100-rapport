%!TEX root = ../main.tex
\chapter[Prosjekt 06: Automatisk kjøring]{Prosjekt 06: Automatisk kjøring {\large (Peter \& Stian)}}
\label{kap:automatisk-kjoring}

\section{Problemstilling}
Etter at roboten hadde blitt kjørt manuelt i kapittel~\ref{kap:manuell}, var det ønskelig å få den til å kjøre automatisk gjennom banen. Fokuset på oppgaven var å få roboten til å kjøre gjennom banen med lavest mulig poengsum, \mintinline{matlab}|Score|, definert i kapittel \ref{sec:p04:loesning}. 

Roboten skal kunne kjøre helt autonomt bare ved å bruke data fra lyssensoren. For å oppnå dette, må det lages en algoritme som regner ut motorpådraget gitt en lysverdi fra lyssensoren.

\section{Forslag til løsning}
Først ble styringslogikken implementert:
\begin{codesample}
PowerA(k) = speed - Styring(k) - Ks*abs(Styring(k));
PowerB(k) = speed + Styring(k) - Ks*abs(Styring(k));

PowerA(k) = max(min(round(PowerA(k)), 100), -100);
PowerB(k) = max(min(round(PowerB(k)), 100), -100);
\end{codesample}
De siste leddene i de to første linjene gjør at roboten bremser ned når den svinger. Hvor mye roboten bremser er bestemt av \mintinline{matlab}|Ks|-faktoren. Variabelen \mintinline{matlab}|Styring| er definert som en vektor slik at den kan plottes senere. Variabelen \mintinline{matlab}|speed| er basisfarten når roboten kjører rett forover.

\subsection{Styring med proporsjonalt motorpådrag}
En enkel løsning er at styringen er lineært avhenging av lysavviket:
\begin{codesample}
Styring(k) = Kp*LysAvvik(end);
\end{codesample}

Hvor \mintinline{matlab}|Kp| er proporsjonalitetsfaktoren. En høy verdi vil føre til at roboten raskt korrigerer for lysavvik. For høye verdier fører til at roboten overkompenserer, og begynner å oscillere. Resultatene fra denne styremåten er presentert i kapittel \ref{sec:propstyring}.


\subsection{PID-kontroller}
\label{sec:pid-contoller}
En normal løsning på slike styringsproblemer er å implementere en såkalt PID-kontroller. Hvordan en PID-kontroller fungerer er illustrert i figur~\ref{fig:pidloop}.

\begin{figure}[hbt]
 \centering
\includegraphics[width=1\textwidth]{images/project06/PID_en}
\caption{Den ønskede verdien, $r(t)$, i vårt tilfelle \mintinline{matlab}|nullpunkt|, og den nåværende verdien $y(t)$ blir kombinert til en avviksverdi $e(t)$. Denne brukes til å kalkulere tre ledd som summeres til $u(t)$, $P$, $I$ og $D$. Denne verdien sendes til prosessen, i vårt tilfelle settes styringen til denne verdien. Den nye målte verdien $y(t)$ blir så brukt til å regne ut $u(t)$ i neste runde. Hentet fra~\cite{fig:pidloop}.}
 \label{fig:pidloop}
\end{figure}

I en PID-kontroller er utgangssignalet lik summen av tre verdier; én som er proposjonal (P) med inngangssignalet, én som er proporsjonal med den integrerte (I) av inngangssignalet, og én som er proporsjonal med den deriverte (D) av inngangssignalet. Det er også her PID-kontrollere får navnet sitt. Gitt ett inngangssignal $i(t)$ er utgangssignalet fra en PID-kontroller 
\begin{align}
u(t) &= K_p i(t) + K_i \int_0^t \! i(\tau) \, \dd \tau + K_d \left. \frac{\partial i}{\partial t} \right|_{t = t}
\end{align}

Hvert ledd i PID-kontrolleren har sitt eget formål:

\begin{itemize}
\item Det proporsjonale leddet bruker den siste målte verdien. 
\item Det deriverte leddet prøver å forutsi den neste målte verdien, og hindrer overkompensering. 
\item Det integrerte leddet ser på alle tidligere målte verdiene og korrigerer for feil som bygger seg opp.
\end{itemize}

\newpage
\subsection{Implementasjon av PID-kontroller}
PID-kontrolleren ble implementert i \verb|P_06_F4_MathCalculations.m|:
\begin{codesample}
LysFiltrert_IIR(k) = IIR_filter(LysFiltrert_IIR(k-1), Lys(k), alpha);
LysAvvik(k) = LysFiltrert_IIR(k)-nullpunkt; 

Ts(k-1) = Tid(k)-Tid(k-1); 
LysIntegrert(k) = EulerForward(LysIntegrert(k-1),LysAvvik(k-1),Ts(k-1));
LysDerivert(k-1) = Derivation(LysFiltrert_IIR(k-1:k),Ts(k-1));

AbsLysAvvik(k)=abs(LysAvvik(k));
AbsLysIntegrert(k) = EulerForward(LysIntegrert(k-1),LysAvvik(k-1),Ts(k-1));
Score(k) = Tid(k)*100 + LysIntegrert(k);

P(k) = Kp*LysAvvik(end);
I(k) = Ki*LysIntegrert(end);
D(k) = Kd*LysDerivert(end);

Styring(k) = P(k) + D(k) + I(k);
\end{codesample} 

Leddene \mintinline{matlab}|P|, \mintinline{matlab}|I|, og \mintinline{matlab}|D| er vektorer slik at de lettere kan plottes senere.

Det finnes flere metoder for å finne gode verdier for parametrene $K_p$, $K_i$ og $K_d$. Det ble forsøkt å bruke Ziegler-Nichols' metode~\cite{Ziegler1942}. Denne metoden bruker periodene til oscillasjonene som oppstår når $K_p$ økes til å regne ut parametrene.


\section{Resultater}

\subsection{Proposjonal styring}
\label{sec:propstyring}
Fullstendige resultater er presentert i tabell \ref{tabell:propstyring}.

\begin{table}[!t]
	\centering
	\caption{Resultater ved proposjonal styring. Tabellen viser verdiene til de to variablene som ble endret, samt den kalkulerte poengsummen etter kjøring. Alle forsøkene ble kjørt med $K_s = 0$, altså ingen bremsing i svingene. Hver kombinasjon av variabler ble kjørt flere ganger fordi ukontrollerte variabler i omgivelsene kan påvirke resultatet. For rader med poengsum ``---'' klarte ikke roboten å komme til mål. I de fleste tilfellene er dette fordi $K_p$ er for lav.}
	\label{tabell:propstyring}
	\begin{tabular}{l l l} \hline
	\mintinline{matlab}|Kp|		& \mintinline{matlab}|speed|		& Poengsum \\ \hline
	0.10	& 10.0		& 2070.8 \\
	0.10	& 10.0		& 2064.4 \\
	0.10	& 10.0		& 1978.9 \\
	0.10	& 10.0		& 2112.7  \\ %# Video #1
	0.10	& 10.0		& 2081.4 \\
	0.04	& 13.6		& --- \\
	0.04	& 13.6		& --- \\
	0.01	& 6.0		& --- \\
	0.01	& 6.0		& --- \\
	0.15	& 8.4		& 2226.3 \\
	0.15	& 8.4		& 2237.4 \\
	0.15	& 8.4		& 2175.3 \\
	0.15	& 8.4		& 2152.3 \\
	0.15	& 8.4		& 2141.2 \\
	0.10	& 15.0		& ---  \\  % Klarer ikke svinger, Kp for lav
	0.10	& 15.0		& --- \\
	0.10	& 15.0		& 1506.4 \\
	0.10	& 15.0		& 1500.3 \\
	0.15	& 15.0		& 1326.7 \\
	0.15	& 15.0		& 1336.0 \\
	0.15	& 20.0		& 1096.3 \\
	0.15	& 20.0		& --- \\
	0.15	& 20.0		& 1070.2  \\ %# Video #2
	0.15	& 25.0		& --- \\
	0.15	& 25.0		& --- \\
	0.20	& 25.0		& --- \\
	0.20	& 25.0		& --- \\
	0.25	& 25.0		& 815.2 \\
	0.25	& 25.0		& 829.7 \\ \hline
	\end{tabular}
\end{table}

For verdier $K_s = 0, K_p = 0.1, speed = 10$ kjører roboten uten problemer gjennom banen. Siden farten er relativt lav, har ikke roboten problemer med å følge linjen. $K_s$ trenger ikke brukes, da roboten ikke har problemer med å følge svingen. For disse verdiene får roboten poengsum $1979$.

Dersom $K_p$ er for lav i forhold til $speed$, klarer ikke roboten å følge med i svingene, svingradiusen blir for høy. Derfor må $K_p$ økes når $speed$ økes. Til slutt nås et punkt der $K_p$ blir så høy at roboten overkompenserer, og det oppstår oscillasjoner rundt den korrekte verdien, og roboten svinger ut av banen.

\begin{sloppypar}
Den beste oppnådde poengsummen ved å bare endre $K_p$ og $speed$ var 815, med \mbox{$Kp=0.25, \, speed=25$}.
\end{sloppypar}

\newpage
\subsection{Styring med PID}
Et plott av variablene $P$, $I$ og $D$ under kjøring er presentert i figur \ref{fig:proj7:PID_drive}.

Først ble PID konstantene funnet med Ziegler-Nichols' metode: $K_p$ ble økt helt til roboten opplevde oscillasjoner. Med $speed=15$ skjedde dette ved $K_p = K_u = 0.23$. Perioden til oscillasjonene målt til å være $T_u = \SI{0.65}{s}$. I følge Ziegler og Nichols kan gode verdier for PID-konstantene regnes ut fra utrykkene i tabell \ref{tabell:zig-nic}.

\begin{figure}[!b]
 \centering
 {\large \includesvg[width=1\textwidth]{images/project01/pidcoeff}}
 \caption{$P$, $I$ og $D$ bidragene under en kjøring gjennom banen. Generelt er det $P$ som dominerer, men $D$ kompenserer i sterke svinger, og under den lange høyresvingen, fra ca. \SI{10}{s} til \SI{17}{s} dominerer $I$, da avviket hele tiden er for høyt i yttersvingen og $I$ bygger seg opp.}
 \label{fig:proj7:PID_drive}
\end{figure}

\begin{table}[hbt]
	\centering
	\caption{Utrykk for gode PID parametre fra Ziegler-Nichols' metode. $K_u$ er $K_p$ når systemet begynner å oscillere. $T_u$ er perioden til disse oscillasjonene. Tabellen gir parametre for både P-, PI- og PID-kontrollere.}
	\label{tabell:zig-nic}
	\begin{tabular}{l l l l} \hline
	Type kontroller 	& $K_p$ 		 & $K_i$ & $K_d$ \\ \hline
	P 	& $0.50 K_u$ 	& ---			 & --- \\
	PI  & $0.45 K_u$	& $0.54 K_u/T_u$ & --- \\
	PID & $0.60 K_u$	& $1.2 K_u/T_u$  & $3K_u T_u/40$ \\ \hline
	\end{tabular}
\end{table}

PID-parametrene ble regnet ut ved $speed=15$ til å være $K_p = 0.138$, $K_i = \SI{0.42}{\per s}$, $K_d = \SI{0.011}{s}$. Roboten kom seg gjennom banen med disse parametrene, og parametrene ble siden justert manuelt etter at $speed$ ble økt for å forbedre poengscoren. Variabelen $K_s$ ble også brukt til å regulere farten i svingene ved høye hastigheter. Resultatene er presentert i tabell \ref{tabell:pidstyring}.

Det beste oppnådde resultatet var en poengsum på 784.

\begin{table}[hbt]
	\centering
	\caption{Resultater ved styring fra PID-kontrolleren. Tabellen viser verdiene til variablene som ble endret, samt den kalkulerte poengsummen etter kjøring. Alle forsøkene ble kjørt med $speed = 30$, som var den høyeste hastigheten som ga pålitelige resultater. Hver kombinasjon av variabler ble kjørt flere ganger fordi ukontrollerte variabler i omgivelsene kan påvirke resultatet. For rader med poengsum ``---'' klarte ikke roboten å komme til mål. Verdien med stjerne er den beste målte verdien.}

	\label{tabell:pidstyring}
	\begin{tabular}{l l l l l} \hline
	\mintinline{matlab}|Kp|	& \mintinline{matlab}|Ki| & \mintinline{matlab}|Kd|	& \mintinline{matlab}|Ks| &
	 Poengsum \\ \hline
	0.25	& 0.55	& 0.000	& 0.0	& --- 	 \\	%# I ble veldig stor i yttersving
	0.25	& 0.55	& 0.000	& 0.0	& ---    \\
	0.25	& 0.55	& 0.000	& 0.0	& ---    \\
	0.25	& 0.10	& 0.010	& 1.0	& 1864.0 \\
	0.25	& 0.10	& 0.001	& 1.0	& 1324.1 \\
	0.25	& 0.10	& 0.001	& 0.2	& ---    \\
	0.25	& 0.10	& 0.001	& 0.5	& 784.3$^\text{*}$  \\
	0.25	& 0.10	& 0.001	& 0.5	& ---    \\
	0.25	& 0.10	& 0.001	& 0.5	& ---    \\
	0.25	& 0.10	& 0.001	& 0.5	& ---    \\
	0.20	& 0.10	& 0.001	& 0.5	& ---    \\
	0.20	& 0.10	& 0.010	& 0.5	& ---    \\
	0.20	& 0.10	& 0.010	& 0.5	& ---    \\
	0.15	& 0.10	& 0.010	& 0.5	& ---    \\
	0.20	& 0.10	& 0.010	& 0.5	& ---    \\
	0.25	& 0.10	& 0.001	& 0.5	& 843    \\ \hline
	\end{tabular}
\end{table}

\subsubsection{Begrensninger med PID-kontroller}
PID-kontrollere, og også Ziegler-Nichols' metode for utregning av PID-parametre har en del begrensninger på hvilke system den fungerer for. Et problem med systemet med roboten er at i en høyresving vil lyssensoren gå fra et helt svart område, til et helt hvitt. Et annet problem er at målefrekvensen er veldig lav. Nye målinger hentes fra lyssensoren med en frekvens på ca.~\SI{40}{Hz}. Dette er veldig lavt, da roboten på denne tiden kan ha beveget seg opptil \SI{2}{cm}. Aggressiv kutting i koden økte målefrekvensen til~\SI{55}{Hz}, men dette er fortsatt lavt.

Leddet $D$ fungerer dårlig når målefrekvensen er lav, da den deriverte av inn-signalet ikke blir glatt nok. Leddet $I$ fungerer heller ikke bra på den banen som ble brukt, da det er en krapp venstresving etter en lang høyresving. Gjennom høyresvingen vil det integrerte leddet $I$ føre til at roboten ønsker å holde seg på høyre siden av nullpunktet. Dersom $K_i$ er for stor kjører roboten utenfor i den neste svingen.


%Automatisk kjøring.
%
%Implementerer en PID kontroller.
%
%Vi bruker Ziegler-Nichols metode for å stemme PID loopen.
%
%% speed 10, Kp = 0.23
%
%% speed 15, Kp = 0.23
%
%% speed 15, Kp = 0.3. Fungerer, men oscillasjoner
%
%%Tu = 0.65
%%Ku = 0.3
%
%
%Ki fungerer ikke bra, da den lange høyresvingen gir et høyt positivt signal, som så fører til at roboten kjører a banen i den neste venstresvingen.
