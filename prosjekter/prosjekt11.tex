%!TEX root = ../main.tex
\chapter[Prosjekt 11: Morse]{Prosjekt 11: Morse {\large (Anders \& Peter)}}
\label{kap:morse}

\newtcbox{\morse}[1][black]{on line, 
            arc=0pt,colback=#1!0!white,colframe=#1!50!black,
            before upper={\rule[-3pt]{0pt}{10pt}},boxrule=1pt,
            boxsep=0pt,left=6pt,right=6pt,top=2pt,bottom=2pt
}
\newcommand{\dt}{\kern-0.5pt\raisebox{0.4ex}{.}}

\section{Problemstilling}
Det var ønskelig å opprette et program som kunne håndtere morsesignaler. Dette programmet skulle kunne oversette alfabetisk tekst til morsesignal i form av både skriftlig, og auditivt resultat. 

Det skulle også kunne oversette auditivt morsesignal til tekst ved hjelp av mikrofon. Dette var ønskelig å kunne gjøre på to måter, mens den mottok signalet (live), og i etterkant når hele beskjeden er mottatt. Programmet skulle klare dette uavhengig av periodefrekvensen til beskjeden. 

Morsekode er et eget alfabet som består av linjer og prikker som symboliserer et langt eller kort signal. Det er en kommunikasjonsmetode som ble brukt til kommunikasjon over lengre distanser, både ved hjelp av store lykter som blir tildekket med intervaller, eller ved hjelp av elektriske signaler, som ble skrudd av og på. Over årene ble det brukt flere forskjellige versjoner av morsealfabetet, men disse er nå i stor grad avløst av et internasjonalt standard-alfabet som vist i figur~\ref{fig:proj11:morse}. 

Som vist i figur~\ref{fig:proj11:morse} er de forskjellige signalene differensiert ut fra hvor lenge de varer, og hvor lange mellomrom det er mellom signalene. Disse tidsperiodene defineres med utgangspunkt i den minste perioden: 
\begin{itemize}
\item Én periode er lengden på et prikk-signal, og lengden på et mellomrom mellom signaler i samme bokstav.
\item Tre perioder er lengden på et linje-signal, og lengden på et mellomrom mellom hver bokstav. 
\item Syv perioder, som er lengden på pauser mellom ord.  
\end{itemize}

\begin{figure}[H]
\centering
{\large \includegraphics[width=\textwidth]{images/project11/International_Morse_Code}}
\caption{Denne figuren viser det internasjonale morsealfabetet, og forklarer hvordan det brukes ved å forklare periodelengder, her kalt ``unit''. Hentet fra~\cite{fig:morse}.}
\label{fig:proj11:morse}
\end{figure}
     
\section{Forslag til løsning}
Først ble det opprettet to funksjoner, en for å oversette alfabetiske tegn til morsekode, og en som oversetter fra en binær verdi som representerer en bokstav i morsekode, til den alfabetiske bokstaven. 

Det ble valgt å bruke binærverdier for å lagre mottatte morsesignaler hvor en dott lagres som 0, og en linje som 1. For å kunne skille mellom \morse{\Large \dt\,-}~=~a, \morse{\Large \dt\,\dt\,-}~=~v, \morse{\Large \dt\,\dt\,\dt\,-}~=~u og tilsvarende tegn som ville fått samme verdi ($01,~001,~0001$) grunnet at de starter med en dott, ble det satt inn et 1 tall i starten slik at alle verdier er unike, som vist i tabellen under.

\begin{tabular}{lll}
\textbf{Bokstav}	&	\textbf{Morse} 	& \textbf{kodet verdi} \\ \hline
a & {\Large \dt\,-} 	 & $01   \rightarrow  101_2=5$ \\
v & {\Large \dt\,\dt\,-}   & $001  \rightarrow 1001_2=9$ \\
u & {\Large \dt\,\dt\,\dt\,-}  & $0001 \rightarrow 10001_2=17$ \\
\end{tabular}

Begge funksjonene tar imot en verdi, og ved hjelp av indeksering ved bruk av \mintinline{matlab}|containers.map| og \mintinline{matlab}|isKey|-funksjonene, gir de ut den tilsvarende verdien. Disse funksjonene fikk navnene \mintinline{matlab}|StringToMorse| (vedlegg~\ref{appendix:code:StringToMorse}) og \mintinline{matlab}|DecodeMorse| (vedlegg~\ref{appendix:code:DecodeMorse}). 

I hovedprogrammet ble det først brukt en meny for å velge hvilken operasjon som ønskes utført. Det ble oppdaget at denne hang seg opp etter valget da den går direkte til all kalkulasjonen som skjer under oversettelsen, og klarer ikke lukke menyvinduet skikkelig. Dette var uønsket, og ble løst ved å legge inn en pause på 10ms etter valget slik som vist under. 
\begin{codesample}
valg=menu('Hvordan ønsker du å oversette?', 'Tekst til morse(skrift)', 'Tekst til morse(lyd)', 'Morse til tekst(etter innspilling)', 'Morse til tekst(live)'); 

pause(0.01); 
\end{codesample}

\subsection{Tekst til morse (skrift)}
\label{kap:proj11:skrift}
Først ble koden for oversettelse fra tekst til skriftlig morse implementert. Her spør programmet brukeren om å oppgi noe som ønskes oversatt til morse. Den bruker så \verb|StringToMorse| for oversettelse i en loop som legger til en streng med det oversatte tegnet for hver bokstav, og til slutt skriver dette ut på skjermen. Når et mellomrom blir oversatt til morsekode, blir den et mellomrom med varighet på 7 perioder. For å illustrere dette skriftlig blir \morse{/} tegnet brukt. 

\subsection{Tekst til morse (lyd)}
\label{kap:proj11:lyd}
Deretter skal den oversatte koden kunne sendes ut ved hjelp av NXT-en. Her ble det tatt utgangpunkt i koden utviklet i kapittel~\ref{kap:proj11:skrift}, men det ble lagt til en ny loop som går gjennom hvert tegn i strengen og gir ut et hørbart lydsignal, hvor periodetid er satt til \SI{75}{ms} og frekvens for lydsignal er satt til \SI{440}{Hz}. Det som måtte passes på her er at det må legges inn pause etter kommandoen er sendt til NXT-en, da programmet bare fortsetter etter den har sendt fra seg kommandoen om frekvens og varighet. 

\subsection{Morse til tekst (etter innspilling)}
Her skal programmet klare å oversette morsekoden til tekst, uavhengig av hvor lang en periode er. For å regne ut periodetiden tar den gjennomsnittsverdien til alle mellomrom mellom signaler som har verdier fra det laveste registrerte mellomrom, og opp til den dobbelte verdien. Dette er en utregning som skjer etter hele beskjeden er mottatt, noe som gjør at programmet må stoppes manuelt ved hjelp av en bryter. Det ble derfor lagt inn en ekstra knapp fra styrestikken for å kunne avslutte måleperioden, som ble definert til \verb|JoyThumbSwitch|.  

Programmet starter ved å lagre alle tidsverdier hvor det blir registrert en positiv og negativ flanke. Disse flankene presenterer start og sluttpunkt for signalene. Den bruker så dette til å beregne tiden det tar mellom hver flanke, ved å lagre dem i en vektor for signal-varighet, og en for signal-mellomrom. 

Etter beskjeden er ferdig, trykkes tommelknapp inn, og programmet beregner ut periodetiden. Deretter startes en loop som går gjennom hvert registrerte signal og oversetter til tekst ved hjelp av \verb|DecodeMorse|-funksjonen, for å til slutt skrive ut den oversatte teksten. 

For å finne ut hvor lenge et signal har vart, sjekker den lengden opp mot periodetiden $+/-$ en satt prosentgrense. Grensen, gitt i prosent, ble definert i variabelen \verb|PT| (prosent-terskel) og brukes til å definere \verb|LimP| og \verb|LimN|, positiv og negativ grense, slik at signalperioden ikke trenger å være helt nøyaktig. 

\begin{codesample}
PT = 40; 
LimP = (1+PT/100); 
LimN = (1-PT/100); 
\end{codesample}

\subsection{Morse til tekst (live)}
Den mest utfordrende delen av prosjektet var å oversette morsesignalet samtidig som det mottas, nok engang uavhengig av periodetiden.  For å gjøre dette bruker den det første ordet til å definere periodetid. Etter dette vil den skrive ut beskjeden hver gang den mottar en ny bokstav. 

I det første ordet blir positiv flanke, negativ flanke, signal-varighet og signal-mellomrom registrert. Den definerer gjennomsnittlig periodetid etter hvert signal som blir registrert, og bruker denne til å se om ordet er ferdig. Dette gjør den ved å sammenligne det siste målte mellomrommet med den antatte periodetiden skalert med 7. Hvis den oppdager at de er tilsvarende like, blir en variabel endret slik at en annen loop tar over oversettelsen ved neste måling. 

Periodetiden er nå ferdig definert, og brukes til å oversette alle nye signal med en gang de mottas slik at de skrives ut med en gang de er mottatt. Oppdager programmet et mellomrom som overstiger 20 ganger størrelsen til en periode etter den siste negative flanken vil det avsluttes.  

\section{Resultat}
De forskjellige delprosjektene i menyen ble testet og virket som ønsket. 

\subsection{Tekst til morse (skrift)} 
I figur~\ref{fig:result:skrift} vises resultatet når delprosjektet kjøres i MATLAB.

\begin{figure}[H]
\centering
\begin{tcolorbox}[colback=white,arc=0mm, width=0.9\textwidth]
\begin{verbatim}
Hva ønsker du å oversettet til morse? dette er resultatet
-.. . - - . / . .-. / .-. . ... ..- .-.. - .- - . -
\end{verbatim}
\end{tcolorbox}
\caption{Her vises setningen ``dette er resultatet'' oversatt til morsekode.}
\label{fig:result:skrift}
\end{figure}

\subsection{Tekst til morse (lyd)}
Dette er en utvidelse av det som er vist i figur~\ref{fig:result:skrift}, og ved satte verdier for frekvens og periodetid som beskrevet i kapittel~\ref{kap:proj11:skrift}, blir resultatet et godt eksempel på hørbar morsekode. 

\subsection{Morse til tekst (etter innspilling)}
På grunn av støy i omgivelsene må terskelen for at målt signal skal bli registrert plasseres nær maks lydvolum. Programmet setter terskelen til en ønsket verdi under høyeste målte volumverdi. Da programmet mottar hele beskjeden før beregninger, er ikke dette et problem. Så lenge alle flanker er riktig registrert, ender resultatet med den riktige beskjeden. 

Signalene som blir registrert av mikrofon holder et fast maksimalt volum i alle signaler. Dette kan observeres i figur~\ref{fig:result:innspilling}, sammen med mellomrom mellom signal. 

\begin{figure}[H]
\centering
\includegraphics[width = \textwidth]{images/project11/Tester_en_to_tre_fin_bok.png}
\caption{Figuren viser volumet som ble registrert. Stolpene utgjør morsesignalet, og tilsvarer bokstavene skrevet over.}
\label{fig:result:innspilling}
\end{figure}

\subsection{Morse til tekst (live)} 
Støy i omgivelsene var et problem her også. Dette gjør at terskelen nok engang settes nær maks volum, men her må den settes manuelt fordi den må kunne skille ut signalene fra omgivelsesstøy fra starten av. Så lenge den sendte beskjeden opprettholder et konstant volum under mottak, klarer programmet å oversette beskjeden til en streng med bokstaver. 

Ved mottak av signalet slik som vist i figur~\ref{fig:result:skrift}, skriver programmet ut setningen hver gang den registrerer et signal, og resultatet blir skrevet ut slik som vist i figur~\ref{fig:result:live}. 

\begin{figure}[H]
\centering
\begin{tcolorbox}[colback=white,arc=0mm, width=0.9\textwidth]
\begin{verbatim}
tester
tester e
tester e
tester en
tester en t
tester en t
tester en t
tester en to 
tester en to t
tester en to t
tester en to t
tester en to tr
tester en to tre
\end{verbatim}
\end{tcolorbox}
\caption{Teksten som blir skrevet ut ved bruk av ``live'' oversettelse av målingen vist i figur~\ref{fig:result:innspilling}}
\label{fig:result:live}
\end{figure}