%!TEX root = ../main.tex
\chapter[Prosjekt 04: Manuell kjøring]{Prosjekt 04: Manuell Kjøring {\large (Hele gruppen)}}
\label{kap:manuell}

\section{Problemstilling}
Målet ved dette prosjektet var å kjøre roboten manuelt rundt en bane (se figur~\ref{fig:kjorebane}) ved hjelp av styrestikken. Roboten er vist i figur~\ref{fig:robot}.

\begin{figure}[hbt]
\centering
\includegraphics[width=1\textwidth]{images/project04/robot.jpg}
\caption{Bilde av roboten på kjørebanen. Fremme på roboten er det festet en lyssensor. Lyssensoren sender ut et lys og måler refleksjonen fra underlaget. Styrestikken som ble brukt for å styre roboten er synlig øverst i bildet.}
\label{fig:robot}
\end{figure}

Under kjøringen ble det hentet inn data fra lys-sensoren. Ved bruk av formelen beskrevet under ble det regnet ut en poengsum. Dette er en beregning som gir poeng basert på hvor nøyaktig roboten holdt seg i midten av banen og hvor lang tid det tok for å komme i mål. 

\begin{codesample}
Score(k) = Tid(k) * 100 + LysIntegrert(k)
\end{codesample}


Verdiene av de deriverte og integrerte lysmålingene ble plottet, og poengsummen til den beste runden ble lagret. De målte verdiene for denne runden er plottet i figur~\ref{fig:proj04:graphs}.

\section{Forslag til løsning}
\label{sec:p04:loesning}
Det ble kjørt et testprogram i MATLAB for å se hvordan aksene på styrestikken blir avlest. Her ble det oppdaget at rotasjonsaksen for styrestikken er definert i MATLAB som ``axis 4''. Denne ble derfor definert i \mintinline{matlab}{P04_F2_GetFirstMeasurement} slik: 

\begin{codesample}
JoyRotate(k)  = -double(joystick.axes(4)); 
\end{codesample}

De avleste verdiene for lys ble brukt til beregninger i \mintinline{matlab}{P04_F4_MathCalculations.m}. Det ble brukt filter for å få en bedre kurve for de deriverte verdiene. 

Da avviket i lysmålingen tar utgangspunkt i midtpunktet i banen som nullpunkt, vil resultatet ha både positive og negative verdier. Det var ønsket at den integrerte kun skal øke med utgangspunkt i hvor langt roboten er fra nullpunktet, uavhengig av hvilken side av midtlinjen den er på. For å gjøre dette ble de integrerte verdiene beregnet ut ifra absoluttverdien av avviket.

\begin{codesample}
Ts(k-1) = Tid(k)-Tid(k-1);  
LysAvvik(k) = Lys(k)-nullpunkt;  
AbsLysAvvik(k)=abs(LysAvvik(k)); 
LysFiltrert_IIR(k) = IIR_filter(LysFiltrert_IIR(k-1), Lys(k), alpha); 
LysIntegrert(k) = Integation(LysIntegrert(k-1),AbsLysAvvik(k-1),Ts(k-1), 'euler'); 
LysDerivert(k-1) = Derivation(LysFiltrert_IIR(k-1:k),Ts(k-1)); 
  
Score(k) = Tid(k)*100 + LysIntegrert(k); 
\end{codesample}
 
Motorkraften ble regnet ut ved å legge sammen verdiene for aksene og skalere dem med en faktor. Dette gjøres fordi aksene på styrestikken gir ut verdier i intervallet $\left(-2^{15}, 2^{15}\right)$, mens motorene kun godkjenner heltallsverdier i intervallet $(-100, 100)$. Faktoren som ble brukt her er $100 \cdot 2^{-16}$. Dette gir verdier som ligger innenfor motorens verdi-intervall. Ligger verdiene utenfor, må disse begrenses til intervallet $(-100, 100)$ ved hjelp av \verb|max|/\verb|min| funksjonene. 

Verdiene ble også avrundet til nærmeste heltall ved hjelp av \verb|round| funksjonen, da motorene kun støtter heltallsverdier. Koden for dette kan skrives slik: 

\begin{codesample}
PowerA(k) = max(min(round(faktor*(JoyForover(k)+JoyRotate(k))),100),-100); 
PowerB(k) = max(min(round(faktor*(JoyForover(k)-JoyRotate(k))),100),-100); 
\end{codesample}
 
Denne koden virker med flere verdier for \verb|faktor|, men dersom denne verdien blir før høy kan det oppstå problemer under svinging. Grunnen til dette er at roboten svinger ved å øke motorpådraget på en av sidene, noe som er umulig dersom begge motorene allerede kjører for fullt. For å løse dette problemet ble det brukt en faktor som kun gir verdier mellom -100 og 100 for motorpådraget. Hver akse på styrestikken vil da kunne gi maksimalt 50~\% av maks hastighet, og det er bare kombinasjonen av svinging og forover kjøring som kan gi et motorpådrag på 100. Da blir roboten lettere å styre, mot at maksimal hastighet begrenses. Når verdier blir begrenset til intervallet $(-100, 100)$ kan koden forenkles til: 

\begin{codesample}
PowerA(k) = round(faktor*(JoyForover(k)+JoyRotate(k))); 
PowerB(k) = round(faktor*(JoyForover(k)-JoyRotate(k))); 
\end{codesample}
 
\section{Resultat}
Resultatet av koden var en robot som var lett å kontrollere. Den ble kjørt rundt banen og informasjonen fra kjøringen ble plottet i figur~\ref{fig:proj04:graphs}. 

\begin{figure}[h!bt]
\centering
\includegraphics[width=1\textwidth]{images/project04/graphs3.png}
\caption[]{Plott fra manuell kjøring rundt banen.

(a): Delfiguren viser hvor roboten befant seg under kjøring. Blått viser målte verdier av lysrefleksjon over tid. Rødt viser en filtrert versjon av den blå kurven. Grønt viser lysverdien for midten av banen.

(b): Delfiguren viser absoluttverdien av målingene, og hvor langt de er fra nullpunktet. 

(c): Delfiguren vises det integrerte resultatet av (b). 

(d): Delfiguren viser beregningen av poengsummen under kjøring. 

(e): Delfiguren viser den deriverte verdien av det filtrerte lyssignalet fra figur (a). 

(f): Delfiguren viser de samme datavektorer som i figur (e), men vist ved hjelp av ``Quiver'' funksjonen.  }
\label{fig:proj04:graphs}
\end{figure}

Som kan sees i figur \ref{fig:proj04:graphs}(b) går verdiene aldri helt ned til null selv om roboten passerer midtlinjen i banen flere ganger. Dette kommer av at samplingfrekvensen fra lyssensoren ikke er tilstrekkelig høy. Roboten har allerede passert midtlinjen når målingen blir tatt, og dette gjør at absoluttverdien aldri når helt ned til null. 

I figur \ref{fig:proj04:graphs}(c) gir den integrerte verdien av målingene i figur \ref{fig:proj04:graphs}(b) en økende verdi som viser arealet under grafen. 

I figur \ref{fig:proj04:graphs}(e) vises den deriverte av lysmålingen, dette gir en graf for hvor rask endringen av målte verdier er. 

Den beste runden, slik som vist i figur \ref{fig:proj04:graphs}(d), ga en poeng score på 599 poeng. 
 
