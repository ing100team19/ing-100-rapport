%!TEX root = ../main.tex
\chapter[Prosjekt 08: Chirp-signal]{Prosjekt 08: Chirp {\large (Kristoffer \& Peter)}}
\label{kap:chirp}

\section{Problemstilling}
Lage et chirp-signal ved å bevege lyssensoren i en sirkel på et gråskalert A4-ark. Hastigheten til rotasjonen ble økt gradvis. Et chirp-signal er et periodisk signal med økende frekvens som illustert i figur~\ref{fig:chirp}.

Lyssignalet ble deretter filtrert av et IIR-filter og sammenlignet i et plott (figur~\ref{fig:proj08:graphs}) med det originale lyssignalet. En annen utfordring med dette prosjektet var å regne ut periodetid og amplitude, noe som skulle plottes på $x$- og $y$-aksen.

Frekvensresponsen til filtrene vi implementerte i \ref{kap:filter} ble utforsket, og plottet.

\begin{figure}[hbt]
\centering
\includegraphics[width=.7\textwidth]{images/project08/innledning.png}
\caption{Eksempel på et chirp-signal: Amplituden holdes konstant og frekvensen øksen gradvis. Hentet fra~\cite{fig:chirp}.}
\label{fig:chirp}
\end{figure}
 
\section{Forslag til løsning}
 
For å analysere frekvensresponsen til filteret blir perioden og amplituden på signalet målt. Nullpunktene blir funnet av koden under.

\begin{codesample}
if LysFiltrert(k) > nullpunkt && LysFiltrert(k-1) < nullpunkt 
    t_null = [t_null Tid(k)]; 
end 
if LysFiltrert(k) < nullpunkt && LysFiltrert(k-1) > nullpunkt 
    t_null = [t_null Tid(k)];
end
\end{codesample}

Koden registrerer begge nullpunktene hver periode, den øverste if-setningen finner punktet der signalet går fra negativt til positivt; og den nederste registrerer overgang fra positiv til negativ. 

For å finne amplituden registreres topp- og bunnpunkter, og amplituden til både det filtrerte og det ufiltrerte signalet blir lagret.

\begin{codesample}
if startet && LysFiltrert(k-1) > LysFiltrert(k-2) && LysFiltrert(k-1) > LysFiltrert(k)
  amplitude_filt = [amplitude_filt abs(LysFiltrert(k-1) - nullpunkt)]; 
  amplitude_raw = [amplitude_raw abs(Lys(k-1) - nullpunkt)]; 
  t_topp = [t_topp Tid(k-1)]; 
end 
 
if startet && LysFiltrert(k-1) < LysFiltrert(k-2) && LysFiltrert(k-1) < LysFiltrert(k) 
  amplitude_filt = [amplitude_filt abs(LysFiltrert(k-1) - nullpunkt)]; 
  amplitude_raw = [amplitude_raw abs(Lys(k-1) - nullpunkt)]; 
  t_bunn = [t_bunn Tid(k-1)]; 
end 
\end{codesample}
Her registrerer den øverste if-setningen toppunkter, og den nederste bunnpunkter. Verdien \mintinline{matlab}|startet = ~isempty(t_null)| forsikrer at ingen amplitudemålinger blir gjort før første nullpunkt er registrert.

For å regne ut frekvensen ved hvert målepunkt brukes den følgende koden:
\begin{codesample}
perioder = (t_null(2:end) - t_null(1:end-1))*2; 
frekvenser = 1./perioder; 
\end{codesample}

Siden tiden mellom to nullpunkter er en halv periode blir denne verdien multiplisert med to.

Dette blir kjørt i \verb|PlottData|-filen da denne er tatt ut av while-loopen, og koden behøver ikke å kjøres flere ganger.

Det viste seg å være utfordrende å få et tilfredsstillende resultat ved bruk av lyssensoren, da amplituden helst skulle holdes helt konstant under målingene. Derfor ble den innebygde sinusfunksjonen brukt til å lage et chirp-signal, for å se hvordan en ``perfekt'' måling ville se ut.

\begin{codesample}
sinus(k) = sin(Tid(k)^1.5);
\end{codesample} 

I dette tilfellet ble MATLAB sin innebygde \mintinline{matlab}|findpeaks| funksjon brukt til å bestemme frekvensen: 
\begin{codesample}
[sinus_amp,sinus_loc] = findpeaks(sinus,Tid(k)); 
[sinus_IIR_amp,sinus_IIR_loc] = findpeaks(sinus_IIR,Tid(k)); 
sinus_perioder = sinus_loc(2:end) - sinus_loc(1:end-1); 
sinus_frekvenser = 1./sinus_perioder; 
ampdiff_sinus = sinus_IIR_amp ./ sinus_amp; 
\end{codesample}

\section{Resultat}
Programmet må kjøres ``offline'' på grunn av måten frekvens blir regnet ut. Dersom det er forskjell i antall målinger mellom nullpunkter og ekstremalpunkter, vil MATLAB ikke klare å plotte resultatet. Når programmet kjøres med lagrede målinger blir figur \ref{fig:proj08:graphs} skrevet ut.

Lineær regresjon ble utført for å finne den generelle trenden i datapunktene. Til dette ble den innebygde funksjonen \mintinline{matlab}|fitlm| brukt.

Dersom målingene var helt perfekte, ville den svarte linjen på figur \ref{fig:proj08:graphs}(c) hatt et stigningstall lik null. Begge plottene viser likevel tydelig at amplituden til den filtrerte signalet synker med stigende frekvens. 

\begin{figure}[hbt]
\centering
\begin{overpic}[width=1\textwidth]{images/project08/double_graph.png}
\put(4,45){\textbf{(a)}}
\put(4,28){\textbf{(c)}}
\put(4,12){\textbf{(e)}}
\put(50,45){\textbf{(b)}}
\put(50,28){\textbf{(d)}}
\put(50,12){\textbf{(f)}}
\end{overpic}
\caption[]{Til venstre vises plottene til signalet fra lyssensoren. Til høyre vises kurven til en sinusfunksjon med økende frekvens. Verdier plottet i svart svarer til det originale signalet. Verdier plottet i rødt er filtrerte verdier. 

(a) og (b): Kurven til chirp-signalet. 

(c) og (d): Amplitude plottet mot frekvens. 

(e) og (f): Relativ amplitude, det vil si amplitude etter filter dividert med den originale amplituden. Her kan fenomenet frekvensrespons observeres, der amplituden til det filtrerte signalet er avhengig av frekvensen.}
\label{fig:proj08:graphs}
\end{figure}

Det som faktisk skjer når et signal blir kjørt gjennom et filter av denne typen er at høyfrekvente signal blir filtrert bort. Dette kan være ønskelig da høyfrekvente signal ofte blir sett på som støy. Denne typen filter kalles gjerne lavpassfilter, som betyr at bare signaler med lav frekvens får passere. Det finnes også høypassfilter som filtrerer bort lave frekvenser fra et signal.

\subsection{Sammenligning av filter}
Etter å ha sett frekvensresponsen til IIR-filteret, var det ønskelig å sammenligne med FIR-filteret, så vel som med forskjellige filterparametre.

I sammenligningen er det to frekvenser som er viktige, signalfrekvensen, og samplingfrekvensen. Dersom samplingfrekvensen er veldig høy, vil filteret kunne holde tritt med høyere signalfrekvens, og vice versa. Derfor ble bare én av disse variert i hver sammenligning.

Den første sammenligningen som ble gjort var mellom IIR- og FIR-filteret på et chirpsignal og er presentert i figur~\ref{fig:freqresp1}. Signalet som ble undersøkt er $\sin(t \cdot f)$, hvor $f$ økte lineært fra $1$ til $18$. Samplingfrekvensen ble holdt konstant på ca.~\SI{32}{Hz}. Denne frekvensen ble valgt fordi den er $100/\pi$~Hz, som er ganske nær samplingfrekvensen fra NXTen til MATLAB. Det observeres at FIR-filterets frekvensrespons avtar raskere enn IIR-filteret, og det er derfor et bedre lavpassfilter. 

\begin{figure}[hbt]
\centering
{\large \includesvg[width=\textwidth]{images/project08/freqresp1}}
\caption[]{Sammenligning av FIR- og IIR-filter frekvensrespons. Det originale signalet er vist i svart, FIR-filteret er vist i rødt, og IIR-filteret er vist i blått.
Signalet er $\sin(t \cdot f)$, hvor $f$ varierer lineært fra 1 til 18.

Envelopen til de filtrerte signalene er plottet med stiplede linjer.

Ved høye signalfrekvenser klarer ikke filteret å ``henge med'', og får en redusert amplitude.
}
\label{fig:freqresp1}
\end{figure}

I den andre sammenligningen ble samplingfrekvensen variert. Resultatet er presentert i figur~\ref{fig:freqresp2}. Her er det to filtrene med forskjellige filterparametre sammenlignet. Øking av samplingfrekvensen er analogt med å senke signalfrekvensen, så selv om den relative amplituden er lavest ved lave samplingfrekvenser er filtrene fortsatt lavpassfiltre. Ved lave samplingfrekvenser oppfører FIR-filtrene seg rart, dette er indikert med vertikale linjer på plottet. I disse punktene tar FIR-filteret gjennomsnittsverdien av en hel periode av sinussignalet. Det filtrerte signaler er da 0. De neste nullpunktene kommer når FIR-filteret spenner to, tre, osv. perioder. Ved lavere samplingfrekvens enn dette ser det ut som om frekvensresponsen øker. Dette er en konsekvens av formen på signalet, og hvordan filteret fungerer, og representerer ikke at filteret faktisk får en bedre frekvensrespons.
\begin{figure}[hbt]
\centering
{\large \includesvg[width=\textwidth]{images/project08/freqresp2}}
\caption[]{Frekvensrespons til filterfunksjonene med varierende filterparametre og samplingfrekvens.

Andreaksen viser amplituden til den filtrerte $\sin$-funksjonen og $x$-aksen viser hvor ofte sinus-funksjonen har blitt samplet.

Ved lave frekvenser vil FIR-filtrene ta gjennomsnittet av en hel sinusperiode, og det filtrerte signalet blir 0. Dette skjer når målefrekvensen er lik $m$. Punktet der dette skjer er markert med vertikale linjer.

Førsteaksen går fra \SI{4}{Hz} til \SI{630}{Hz}.}
\label{fig:freqresp2}
\end{figure}
