%!TEX root = ../main.tex
\chapter[Prosjekt 01: Integrasjon]{Prosjekt 01: Integrasjon {\large (Hele gruppen)}}
\label{kap:integrasjon}
To funksjoner for numerisk integrasjon av signal ble implementert; rektangelmetoden og trapesmetoden. Disse funksjonene er lagt ved i vedlegg~\ref{appendix:code:int}.

Av disse er trapesmetoden den mest nøyaktige, på bekostning av høyere kompleksitet.

Funksjonene ble verifisert med lyssensor-data, og sammenlignet mot hverandre.

\section{Rektangelmetoden}
Rektangelmetoden er illustrert i figur~\ref{fig:int:rect}. Metoden er blant de enkleste numeriske integrasjonsmetodene, og kan ses på som å bytte ut den infinitesimale $\dd t$ delen i en integrasjon med et diskret steg, $\Delta t$, slik at
\begin{align}
\int_{t_0}^{t_1} \! f(t) \, \dd t &\approx \sum_{i} \Delta t_i f(t_i) {\rm ,}
\end{align}
hvor $f(t_1), f(t_2), \ldots$ er målepunkter, og $\Delta t_1, \Delta t_2, \ldots$ er tidsintervallet mellom de siste to målingene:
\begin{align}
\Delta t_i &= t_i - t_{i-1}
\end{align}

\begin{figure}[!t]
\centering
{\large \includesvg[width=.45\textwidth]{images/project01/rectangle_integration}}
\caption{Integrasjon basert på rektangelmetoden. Modifisert fra~\cite{fig:integration}.}
\label{fig:int:rect}
\end{figure}

Denne metoden er også kalt Riemann-summering~\cite{riemann}. I praksis kan høyden på rektanglene tas ved starten, slutten, eller i midten av måleområdet. Den første og siste av disse valgene vil overestimere integralet for henholdsvis synkende og økende funksjoner, og den tredje er litt vanskeligere å implementere. Dersom høydene til rektanglene blir satt til verdien på venstre side blir metoden også kalt Eulers forovermetode~\cite{oppgavetekst}.

Det ble valgt å sette høyden på kvadratet til verdien på høyre side, som illustrert i figur~\ref{fig:int:rect}.

\newpage
\section{Trapesmetoden}
En forbedring i forhold til rektangelmetoden er trapesmetoden, illustrert i figur~\ref{fig:int:trap}. Hvor rektangelmetoden bare er korrekt over konstante deler av en funksjon, er trapesmetoden korrekt over alle lineære intervaller. Dette blir illustrert når metodene blir sammenlignet i kapittel \ref{kap:int:comp}.

Trapesmetoden summerer arealet til trapesene under grafen og kan skrives

\begin{align}
\int_{t_0}^{t_1} \! f(t) \, \dd t &\approx \sum_i (t_i - t_{t-1}) \frac{f(t_{i-1}) - f(t_{i})}{2} 
= \frac{1}{2}\Delta t_i \Delta f(t_i) {\rm ,}
\end{align}

\begin{figure}[!b]
\centering
{\large \includesvg[width=.45\textwidth]{images/project01/trapezoid_integration}}
\caption{Integrasjon basert på trapesmetoden. Modifisert fra~\cite{fig:integration}.}
\label{fig:int:trap}
\end{figure}

\section{Implementasjon av koden}
MATLAB-koden ble først skrevet direkte inn i \verb|P01_F4_MathCalculations.m|, hvor den så omtrent slik ut:
\begin{codesample}
LysIntegrert(k) = LysIntegrert(k-1) + LysAvvik(k-1)*Ts(k-1);
\end{codesample}
for rektangelmetoden, og
\begin{codesample}
LysIntegrert(k) = LysIntegrert(k-1) + (LysAvvik(k-1)+LysAvvik(k-2))*Ts(k-1);
\end{codesample}

Deretter ble koden for de to integrasjonsmetodene kombinert og flyttet til en egen funksjon. Variabelnavnene ble generalisert for bruk på vilkårlige signal. Denne funksjonen er inkludert i vedlegg~\ref{appendix:code:int}.

\section{Integrasjon av en konstant verdi}
For å sjekke om den implementerte koden fungerte ble det gjennomført en test. I denne testen ble lyssensoren fra legosettet ført over gråtoneskalaen på kjørebanen, se figur~\ref{fig:kjorebane}(a).

Ved integrasjon av en konstant verdi $a$ blir resultatet en rett linje med stigningstall $a$. Dette kan uttrykkes som
\begin{align}
\int_0^t \! a \, \dd t &= a \cdot t {\rm .}
\end{align}

For å teste dette ble lyssensoren holdt over de grå feltene på gråtoneskalaen øverst på kjørebanen, se figur~\ref{fig:kjorebane}(a). Råsignalet fra lyssensoren, samt det integrerte verdien til $LysAvvik$, definert som 
\begin{align}
LysAvvik(t) = Lys(t)-Lys(0) {\rm ,}
\end{align}
vises i figur~\ref{fig:int:const-int}. Som kan sees fra figuren korresponderer et flatt lyssignal til en rett linje på grafen for det integrerte signalet. Når $LysAvvik$ er negativ, har det integrerte signalet en negativ stigning, som forventet.

Verdien til de avvikene til signalet ble beregnet til å være svært lik verdien til stigningstallet til den integrerte over samme intervall.

\begin{figure}[!t]
\centering
{\large \includesvg[width=1\textwidth]{images/project01/graph}}
\caption{Resultat fra verifiseringen av integrasjonskoden. Den øverste grafen viser rådata fra lyssensoren, justert slik at første verdi er null. Den nederste grafen viser det integrerte verdien til $LysAvvik$. De markerte arealene er regnet ut, og er svært lik verdiene til den integrerte over de samme intervallene.}
\label{fig:int:const-int}
\end{figure}

\section{Sammenligning av rektangel- og trapesmetoden}
\label{kap:int:comp}
De to metodene for numerisk integrasjon som ble implementert er ofte vanskelige å skille fra hverandre ved normal bruk, men fordelen med trapesmetoden kan illustreres med integrasjon av signaler som er stykkevis lineære. Som eksempel ble et trekantsignal integrert med de to metodene. Trekanstsignalet kan integreres analytisk, og resultatet fra rektangel- og trapesmetoden ble sammenlignet med den analytiske verdien. Resultatet er presentert i figur~\ref{fig:int:comp-trekant}. 

\begin{figure}[!b]
\centering
{\large \includesvg[width=1\textwidth]{images/project01/comp_tri}}
\caption{Resultat ved sammenligning av integrasjonsmetodene på et trekantsignal. Det observeres liten forskjell mellom de to integrerte funksjonene og den analytiske løsningen. Dersom det gjøres målinger akkurat på ``knekkene'' ved $x=0.5$ og $x=1.5$ vil den absolutte feilen til trapesmetoden være null relativt til den analytiske løsningen. Feilen til rektangelmetoden er negativ når signalet er økende, og motsatt.}
\label{fig:int:comp-trekant}
\end{figure}

For stykkevis lineære funksjoner vil trapesmetoden gi nøyaktig verdi. Hvordan stigningstallet påvirker feilen til rektangelmetoden kan også observeres. Siden det er ``høyre'' rektangelmetode som blir brukt vil verdien for den integrerte undervurderes når signalet stiger, og undervurderes når signalet synker i verdi.

For å se på mer realistiske signaler ble metodene også sammenlignet på en sinusfunksjon, og sammenlignet med den nøyaktige verdien $1-\cos(t)$ for integralet. Resultatet er presentert i figur~\ref{fig:int:comp-sin}. Dersom feilen, $E(f)$, til en integrasjonsmetode blir definert som
\begin{align}
E(f) &= N(f;a,b) - \int_a^b \! f(\tau) \, \dd \tau {\rm ,}
\end{align}
hvor $N(f;a,b)$ er den numeriske integrasjonen av $f$ i intervallet $a$ til $b$, er feilen til rektangelmetoden og trapesmetoden henholdsvis~\cite{cs411:lecturenote:18}
\begin{align}
E_R(f) &= -\frac{f'(b)}{2}(b-a)^2 \\
\intertext{og}
E_T(f) &= -\frac{f''(b)}{12}(b-a)^3 {\rm .}
\end{align}

Uttrykkene gjelder for hvert målepunkt, og er feilen for hver metode fra tid $a=t$ til $b=t+1$. Feilen som observeres på grafene i dette delkapittelet er den integrerte feilen over alle målepunktene. Det er derfor feilen til rektangelmetoden i begge grafene er $\propto -f$, hvor f er signalet, og ikke $\propto - f'$, som utrykket over virker å antyde.

\begin{figure}[hbt]
\centering
{\large \includesvg[width=1\textwidth]{images/project01/comp}}
\caption{Resultat ved sammenligning av integrasjonsmetodene på et sinussignal. Det observeres liten forskjell mellom de to integrerte funksjonene og den analytiske løsningen. Absoluttfeilen til trapesmetoden er forstørret ti ganger for lettere sammenligning. Feilen til rektangelmetoden er negativ når signalet er økende, og vice versa. Feilen til trapesmetoden faller når kurvaturen til signalet er positiv.}
\label{fig:int:comp-sin}
\end{figure}

Begge disse utrykkene kan observeres i figuren. Absoluttfeilen til rektangelmetoden synker når signalet øker, og motsatt. Absoluttfeilen til trapesmetoden synker når kurvaturen, dvs. den dobbeltderiverte, til signalet er positiv, i intervallet $[\pi, 2 \pi]$. 

Det finnes også numeriske integrasjonsmetoder hvor feilen kun er avhengig av $f^{(3)}$, som for eksempel Simpson's metode som kan generaliseres til å fungere på data tatt opp med ujevne tidsintervall~\cite{reed2014numerically}. Disse vil være nøyaktige også for andregrads polynomer.