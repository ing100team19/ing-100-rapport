% Vent litt før målinger begynner. Dette forhindrer indekseringsfeil
if k>5
	LysFiltrert_IIR(k) = IIR_filter(LysFiltrert_IIR(k-1), Lys(k), alpha);

	if LysFiltrert_IIR(k) > nullpunkt && LysFiltrert_IIR(k-1) < nullpunkt
		t_null = [t_null Tid(k)];
	end
	if LysFiltrert_IIR(k) < nullpunkt && LysFiltrert_IIR(k-1) > nullpunkt
		t_null = [t_null Tid(k)];
	end
	% ~isempty(t_null) sjekker at den første perioden har startet, dvs. "støy" før måling starter blir tatt bort
	if ~isempty(t_null) && LysFiltrert_IIR(k-1) > LysFiltrert_IIR(k-2) && LysFiltrert_IIR(k-1) > LysFiltrert_IIR(k)
		amplitude_filt = [amplitude_filt abs(LysFiltrert_IIR(k-1)-nullpunkt)];
		amplitude_raw = [amplitude_raw abs(Lys(k-1)-nullpunkt)];
		t_topp = [t_topp Tid(k-1)];
	ends
	% Her blir begge nullpunkt i hver periode registert, dette blir korrigert for senere
	if ~isempty(t_null) && LysFiltrert_IIR(k-1) < LysFiltrert_IIR(k-2) && LysFiltrert_IIR(k-1) < LysFiltrert_IIR(k)
		amplitude_filt = [amplitude_filt abs(LysFiltrert_IIR(k-1)-nullpunkt)];
		amplitude_raw = [amplitude_raw abs(Lys(k-1)-nullpunkt)];
		t_bunn = [t_bunn Tid(k-1)];
	end
end