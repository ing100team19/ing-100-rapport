perioder = (t_null(3:end) - t_null(2:end-1))*2;
frekvenser = 1./perioder;

% Lineær regressjon:
% aRaw=fitlm(frekvenser,amplitude_raw(2:end-1));
% bFilt=fitlm(frekvenser,amplitude_filt(2:end-1));
% x1=aRaw.Coefficients(2,1);
% x2=bFilt.Coefficients(2,1);
% a1=aRaw.Coefficients(1,1);
% a2=aFilt.Coefficients(1,1);

x1=-2.5309;
a1=14.1674;
x2=-4.0668;
a2=11.855;

aCalc = a1+frekvenser*x1;
bCalc = a2+frekvenser*x2;

figure(fig1)
subplot(2,1,1)
plot(Tid(1:k),Lys(1:k), 'k',Tid(1:k), LysFiltrert_IIR(1:k), 'r'); 
legend('Målt', 'IIR-filtrert');
xlabel('Tid [sek]')
title('Chirp Signal')

subplot(2,1,2)
plot(frekvenser, amplitude_raw(2:end-1),'k*', frekvenser, amplitude_filt(2:end-1), 'r*', frekvenser, aCalc,'k',frekvenser, bCalc,'r')
xlabel('Frekvens')
ylabel('Amplitude')
legend('Målt', 'IIR-filtrert')

drawnow

