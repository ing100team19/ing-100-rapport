figure(fig1)
subplot(3,2,5)
plot(Tid(1:k),PowerA(1:k), 'b');
xlabel('Tid [sek]')
ylabel('Prosent [%]')
title('Motorpådrag')

subplot(3,2,3)
plot(Tid(1:k),AvstandAvvik(1:k), 'b');
xlabel('Tid [sek]')
ylabel('[cm]')
title('Avvik')

subplot(3,2,1)
plot(Tid(1:k),AvstandFiltrert_IIR(1:k), 'b', Tid(1:k), ones(size(Tid(1:k)))*nullpunkt),'g'; 
xlabel('Tid [sek]')
ylabel('[cm]')
title('Avstand')

subplot(3,2,2)
plot(Tid(1:k),PosMmA(1:k)/10,'b');
xlabel('Tid [sek]')
ylabel('[cm]')
title('Posisjon')

subplot(3,2,4)
plot(Tid(1:k-1),PosDerivertA(1:k-1)/10,'b');
xlabel('Tid [sek]')
ylabel('[cm/s]')
title('Fart')

subplot(3,2,6)
plot(Tid(1:k-1), P_vektor(1:k-1), Tid(1:k-1), I_vektor(1:k-1), Tid(1:k-1), speed_vektor(1:k-1));
legend('P','I','total')
xlabel('Tid [sek]')
title('PI-kontroller')

drawnow

