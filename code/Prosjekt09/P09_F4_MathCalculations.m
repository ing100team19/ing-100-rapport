AvstandFiltrert_IIR(k) = IIR_filter(AvstandFiltrert_IIR(k-1), Avstand(k), alpha);
AvstandAvvik(k) = AvstandFiltrert_IIR(k)-nullpunkt; % beregn avvik fra nullpunkt
AbsAvstandAvvik(k) = abs(AvstandAvvik(k));
Ts(k-1) = Tid(k) - Tid(k-1); % beregn tidsskritt i sekund
AvstandIntegrert(k) = Integration(AvstandIntegrert(k-1), AvstandAvvik(k-1), Ts(k-1), 'euler');
AvstandDerivert(k-1) = Differentiation(AvstandFiltrert_IIR(k-1:k), Ts(k-1));

%Posisjon av motorer målt i mm fra null
PosMmA(k)=(PosMotorA(k)/360*185);
%Filtert verdi posisjon i mm
PosMmAFilt(k)=IIR_filter(PosMmAFilt(k-1),PosMmA(k), alpha);
%Derivert verdi av posisjon til motor
PosDerivertA(k-1)=Derivation(PosMmAFilt(k-1:k),Ts(k-1));
%Dobbelderivert verdi av posisjon til motor
if k>2
    PosDobDerivertA(k-2)=Derivation(PosDerivertA(k-2:k-1),Ts(k-1));
end
