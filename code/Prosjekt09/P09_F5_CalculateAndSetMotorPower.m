P = Kp.*AvstandAvvik(end);
I = Ki.*AvstandIntegrert(end);
D = Kd.*AvstandDerivert(end);

P_vektor = [P_vektor P];
I_vektor = [I_vektor I];
D_vektor = [D_vektor D];

speed = P + I + D;

speed_vektor = [speed_vektor speed];

PowerA(k) = max(min(round(speed),100),-100);
PowerB(k) = max(min(round(speed),100),-100);

if online
    motorA.Power = PowerA(k);
    motorB.Power = PowerB(k);
   
    motorA.SendToNXT();
    motorB.SendToNXT();
end
