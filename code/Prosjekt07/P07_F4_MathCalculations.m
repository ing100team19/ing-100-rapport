Ts(k-1) = Tid(k)-Tid(k-1);

%Posisjon av motorer målt i mm fra null
PosMmA(k) = (PosMotorA(k)/360*185);
PosMmB(k) = (PosMotorB(k)/360*185);

%Filtert verdi posisjon i mm
PosMmAFilt(k) = IIR_filter(PosMmAFilt(k-1),PosMmA(k),alpha);
PosMmBFilt(k) = IIR_filter(PosMmBFilt(k-1),PosMmB(k),alpha);

%Derivert verdi av posisjon til motor A
PosDerivertA(k) = Derivation(PosMmAFilt(k-1:k),Ts(k-1));
PosDobDerivertA(k) = Derivation(PosDerivertA(k-1:k),Ts(k-1));

%Derivert verdi av posisjon til motor B
PosDerivertB(k) = Derivation(PosMmBFilt(k-1:k),Ts(k-1));
PosDobDerivertB(k) = Derivation(PosDerivertB(k-1:k),Ts(k-1));

%Akselerometer verdi i mm, og filtrert verdi
AksMm(k) = Aks(2,k)*9.81;
AksMmFilt(k) = IIR_filter(AksMmFilt(k-1),AksMm(k),alpha);

%Integrert verdi av filtrert akselerometer ved hjelp av Euler
AksIntegrertEuler(k) = Integration(AksIntegrertEuler(k-1),AksMmFilt(k),Ts(k-1),'euler');
AksDobIntegrertEuler(k) = Integration(AksDobIntegrertEuler(k-1),AksIntegrertEuler(k),Ts(k-1),'euler');

%Integrert verdi av filtrert akselerometer ved hjelp av Trapes
AksIntegrertTrapes(k) = Integration(AksIntegrertTrapes(k-1),AksMmFilt(k-1:k),Ts(k-1),'trapes');
AksDobIntegrertTrapes(k) = Integration(AksDobIntegrertTrapes(k-1),AksIntegrertTrapes(k-1:k),Ts(k-1),'trapes');