figure(fig1)

subplot(3,1,1)
plot(Tid(1:k),PosMmA(1:k),'b',Tid(1:k),AksDobIntegrertEuler(1:k),'k',Tid(1:k),AksDobIntegrertTrapes(1:k),'--r');
xlabel('Tid [sek]')
ylabel('mm')
title('Posisjon')
legend('Målt fra motor','Euler-dobbelintegrert fra akselerometer','Trapes-dobbelintegrert fra akselerometer','Location','NorthWest')

subplot(3,1,2)
plot(Tid(1:k),PosDerivertA(1:k),'b',Tid(1:k),AksIntegrertEuler(1:k),'k',Tid(1:k),AksIntegrertTrapes(1:k),'--r');
xlabel('Tid [sek]')
ylabel('mm/s')
title('Fart')
legend('Derivert fra motor','Euler-integrert fra akselerometer','Trapes-integrert fra akselerometer','Location','NorthWest')

subplot(3,1,3)
plot(Tid(1:k),PosDobDerivertA(1:k),'b',Tid(1:k),AksMmFilt(1:k),'g'); 
xlabel('Tid [sek]')
ylabel('mm/s^2')
title('Akselerasjon')
legend('Dobbelderivert fra motor','Målt fra akselerometer','Location','NorthWest')

drawnow

