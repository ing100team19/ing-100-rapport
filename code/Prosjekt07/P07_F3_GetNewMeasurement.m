k=k+1;  
if online
    Tid(k) = toc;                

    Tilt(:,k) = double(GetAbsoluteIMU_Tilt(SENSOR_4));   
    Aks(:,k)  = double(GetAbsoluteIMU_Acc(SENSOR_4));

    InfoMotorA = motorA.ReadFromNXT();  
    InfoMotorB = motorB.ReadFromNXT();  
        
    PosMotorA(k) = InfoMotorA.Position;
    PosMotorB(k) = InfoMotorB.Position;
    
    joystick      = joymex2('query',0);  
    JoyMainSwitch = joystick.buttons(1);
    JoyForover(k) = -double(joystick.axes(2)); 
else
    if k==numel(Ts)
        JoyMainSwitch=1; 
    end
end
