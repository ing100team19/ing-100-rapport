%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Først ble funksjonaliteten til filtrerene implementert
%	direkte inn i MathCalculations.m, deretter ble det lagt til
%	et kall til funksjoner som ble konstruerte.
%
%%%%%%%%%%%%%%%%%%%%%%%%~~Før~~%%%%%%%%%%%%%%%%%%%%%%%%%%
if k+1-m<1
	LysFiltrert_FIR(k) = LysFiltrert_FIR(k-1);
else
	LysFiltrert_FIR(k) =sum(Lys(k+1-m:k))/m;
end

LysFiltrert_IIR(k) =alpha*Lys(k)+(1-alpha)*LysFiltrert_IIR(k-1);

%%%%%%%%%%%%%%%%%%%%%%%%~~Etter~~%%%%%%%%%%%%%%%%%%%%%%%%

LysFiltrert_FIR(k) = FIR_filter(Lys(1:k), m);
LysFiltrert_IIR(k) = IIR_filter(LysFiltrert_IIR(k-1), Lys(k), alpha);
