k=k+1;  

if online
    Tid(k) = toc;                

    Lys(k) = GetLight(SENSOR_1); 

    joystick = joymex2('query',0);  
    JoyMainSwitch = joystick.buttons(1);
    JoyForover(k) = -double(joystick.axes(2)); 
    
    JoyRotate(k) = -double(joystick.axes(4));
else

    if k==numel(Lys)
        JoyMainSwitch=1; 
    end
end
