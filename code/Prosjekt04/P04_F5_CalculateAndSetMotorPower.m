PowerA(k) = round(faktor*JoyForover(k)+faktor*JoyRotate(k)/faktorcomp);
PowerB(k) = round(faktor*JoyForover(k)-faktor*JoyRotate(k)/faktorcomp);

if online
    motorA.Power = PowerA(k);
    motorB.Power = PowerB(k);    
    
    motorA.SendToNXT();
    motorB.SendToNXT();
else
    pause(0.01) 
end
