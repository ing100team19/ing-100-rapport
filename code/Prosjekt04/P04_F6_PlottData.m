figure(fig1)
subplot(3,2,1)
plot(Tid(1:k),Lys(1:k),'b',Tid(1:k),ones(size(Tid(1:k)))*nullpunkt,'g',Tid(1:k),LysFiltrert_IIR(1:k),'r');
xlabel('Tid [sek]')
title('Lys')
legend('Lys','Midtpunkt','Lys(Filtrert)','Location','Northwest')

subplot(3,2,2)
plot(Tid(1:k),AbsLysAvvik(1:k),'-ob');
xlabel('Tid [sek]')
title('Lys Avvik')
grid on

subplot(3,2,3)
plot(Tid(1:k),LysIntegrert(1:k),'b');
xlabel('Tid [sek]')
title('Lys Avvik Integrert ')

subplot(3,2,4)
plot(Tid(1:k),Score(1:k),'b');
xlabel('Tid [sek]')
title('Score')
grid on

subplot(3,2,5)
plot(Tid(1:k-1),LysDerivert(1:k-1),'b'); 
xlabel('Tid [sek]')
title('Lys Derivert')

subplot(3,2,6)
quiver(Tid(1:k-1),LysDerivert(1:k-1),'b'); 
xlabel('Tid [vilkårlig enhet]')
title('Lys Derivert')

drawnow

