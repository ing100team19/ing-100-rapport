Ts(k-1) = Tid(k)-Tid(k-1);      % beregn tidsskritt i sekund
LysAvvik(k) = Lys(k)-nullpunkt; % beregn avvik fra nullpunkt
AbsLysAvvik(k)=abs(LysAvvik(k));
LysFiltrert_IIR(k) = IIR_filter(LysFiltrert_IIR(k-1), Lys(k), alpha);
LysIntegrert(k) = EulerForward(LysIntegrert(k-1),AbsLysAvvik(k-1),Ts(k-1));
LysDerivert(k-1) = Derivation(LysFiltrert_IIR(k-1:k),Ts(k-1));

Score(k) = Tid(k)*100 + LysIntegrert(k);