figure(fig1) 

subplot(4,1,1)
[ax,h1,h2] = plotyy(Tid(1:k),UltraSonic(1:k), Tid(1:k), Lys(1:k));
set(ax(1),'YTick',[10: 10: 45])
set(ax(2),'YTick',[300: 100: 800])
set(ax,'XLim',[0 Tid(end)])
xlabel('Tid [sek]')
ylabel(ax(1),'Sonic, [mm]')
ylabel(ax(2),'Lys,[Arb.u]')
title('Ultrasonic og Light')
legend('UltraSonic','Lys','Location','NorthWest')

subplot(4,1,2)
plot(Tid(1:k),Distanse(1:k)); 
xlabel('Tid [s]')
ylabel('Distanse, [m]')
title('Distanse')
legend('Distanse[m]','Location','NorthWest')
axis([0 Tid(end) (min(Distanse)-0.2) (max(Distanse)+0.2)])

subplot(4,1,3)
plot(Tid(1:k-1),(FartA(1:k-1)/1000)); 
xlabel('Tid, [s]')
ylabel('Hastighet, [m/s]')
title('Hastighet')
legend('Hastighet','Location','NorthWest')
axis([0 Tid(end) (min(FartA/1000)-0.2) (max(FartA/1000)+0.2)])

subplot(4,1,4)
plot(Tid(1:k-2),(AkslerasjonA(1:k-2)/1000)); 
xlabel('Tid, [s]')
ylabel('Akselerasjon, [m/s^2]')
title('Akselerasjon')
legend('Akselerasjon','Location','NorthWest')
axis([0 Tid(end) (min(AkslerasjonA/1000)-0.2) (max(AkslerasjonA/1000)+0.2)])

drawnow