Ts(k-1) = Tid(k)-Tid(k-1);

%Posisjon av motorer målt i mm fra null
PosMmA(k)=(PosMotorA(k)/360*845); 

%Filtrert verdi posisjon i mm
PosMmAFilt(k)=IIR_filter(PosMmAFilt(k-1),PosMmA(k),alpha);

%Distanse tilbakelagt(i meter)
Distanse(k) = PosMmA(k)/1000;
DistanseFiltrert(k) = PosMmAFilt(k)/1000;

%Derivert verdi av posisjon til motor A
FartA(k)=Derivation(PosMmAFilt(k-1:k),Ts(k-1));
AkslerasjonA(k)=Derivation(FartA(k-1:k),Ts(k-1));

UltraSonicFiltrert_IIR(k) = IIR_filter(UltraSonicFiltrert_IIR(k-1), UltraSonic(k), alpha);
if k>3
    if UltraSonic(k-1) < UltraSonic(k-2) && UltraSonic(k-1) < UltraSonic(k) ...
            && UltraSonic(k-1) < 20 && (Tid(k) - UStid) > 0.2
        UStid = Tid(k);
        UStidV = [UStidV UStid];
        USrunde = USrunde + 1; 
        USrundeV = [USrundeV USrunde]; 
    end
   
    if Lys(k-1) < Lys(k-2) && Lys(k-1) < Lys(k) && Lys(k-1) < 450 ...
            && (Tid(k) - Ltid) > 0.2
        Ltid = Tid(k);
        LtidV = [LtidV Ltid];
        Lrunde = Lrunde + 1; 
        LrundeV = [LrundeV Lrunde]; 
    end
    
    if Lrunde > 0 && USrunde > 0 && UStidV(USrunde) - LtidV(Lrunde) > 0 ...
            && (UStidV(USrunde) - LtidV(Lrunde)) <  0.3  
        PosRunde(USrunde) = 1.1624;     
        
    elseif USrunde > 0 && Lrunde > 0 && UStidV(USrunde) - LtidV(Lrunde) < 0 ...
           && (LtidV(Lrunde) - UStidV(USrunde)) < 0.3   
        PosRunde(USrunde) = -1.1624;
    end
end