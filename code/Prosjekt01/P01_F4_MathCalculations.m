Ts(k - 1) = Tid(k) - Tid(k - 1);             % beregn tidsskritt i sekund
LysAvvik(k) = Lys(k) - nullpunkt;            % beregn avvik fra nullpunkt
LysIntegrert(k) = Integration(LysIntegrert(k-1), LysAvvik(k-1), Ts(k-1), 'euler');