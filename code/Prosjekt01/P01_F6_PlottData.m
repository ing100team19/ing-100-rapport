figure(fig1)

subplot(3,1,1)
plot(Tid(1:k),Lys(1:k)); 
xlabel('Tid [sek]')
title('Lys')

subplot(3,1,2)
plot(Tid(1:k),LysAvvik(1:k)); 
xlabel('Tid [sek]')
title('Lys Avvik')

subplot(3,1,3)
plot(Tid(1:k),LysIntegrert(1:k))
xlabel('Tid [sek]')
title('LysIntegrert (integralet av LysAvvik)')

drawnow