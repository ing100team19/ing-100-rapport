k=k+1;  

if online
    %------------------------------------------------
    % tid gått siden første måling
    %------------------------------------------------
    Tid(k) = toc;                

    Lys(k)     = GetLight(SENSOR_1);     

    joystick      = joymex2('query',0);  
    JoyMainSwitch = joystick.buttons(1);
else
    if k==numel(Lys)
        JoyMainSwitch=1; % Simuler joystick knapp
    end
end