k=k+1;  
if online
    Tid(k) = toc;                
    Lys(k) = GetLight(SENSOR_1);       

    % Knappen brukes for å registrere at roboten kommer i mål
    button_pressed = GetSwitch(SENSOR_2);

    joystick      = joymex2('query',0);  
    JoyMainSwitch = joystick.buttons(1);
else
    if k==numel(Lys)
        JoyMainSwitch=1; 
    end
end
