figure(fig1)
subplot(3,2,1)
plot(Tid(1:k),Lys(1:k),'b',Tid(1:k),ones(size(Tid(1:k)))*nullpunkt,'g',Tid(1:k),LysFiltrert_IIR(1:k),'r');
xlabel('Tid [sek]')
title('Lys')

subplot(3,2,2)
plot(Tid(1:k),AbsLysAvvik(1:k));
xlabel('Tid [sek]')
title('Lys Avvik')

subplot(3,2,3)
plot(Tid(1:k),LysIntegrert(1:k));
xlabel('Tid [sek]')
title('Lys Avvik Integrert ')

subplot(3,2,4)
plot(Tid(1:k),Score(1:k));
xlabel('Tid [sek]')
title('Score')

subplot(3,2,5)
plot(Tid(1:k-1),LysDerivert(1:k-1)); 
xlabel('Tid [sek]')
title('Lys Derivert')


subplot(3,2,6)
plot(Tid(1:k-1),P(1:k-1),Tid(1:k-1),I(1:k-1),Tid(1:k-1),D(1:k-1))
xlabel('Tid [sek]')
title('P I D values')
legend('P', 'I', 'D')

drawnow

