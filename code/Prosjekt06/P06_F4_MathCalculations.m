LysFiltrert_IIR(k) = IIR_filter(LysFiltrert_IIR(k-1), Lys(k), alpha);
LysAvvik(k) = LysFiltrert_IIR(k)-nullpunkt; 

Ts(k-1) = Tid(k)-Tid(k-1); 
LysIntegrert(k) = EulerForward(LysIntegrert(k-1),LysAvvik(k-1),Ts(k-1));
LysDerivert(k-1) = Derivation(LysFiltrert_IIR(k-1:k),Ts(k-1));

AbsLysAvvik(k)=abs(LysAvvik(k));
AbsLysIntegrert(k) = EulerForward(LysIntegrert(k-1),LysAvvik(k-1),Ts(k-1));
Score(k) = Tid(k)*100 + AbsLysIntegrert(k);

% Kalkuler PID bidrag

P(k) = Kp*LysAvvik(end);
I(k) = Ki*LysIntegrert(end);
D(k) = Kd*LysDerivert(end);

Styring(k) = P(k) + D(k) + I(k);
