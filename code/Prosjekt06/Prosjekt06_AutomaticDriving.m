clear; close all        % Alltid lurt å rydde opp først
online=1;        		% Er du koplet til NXT eller ikke?
filename = '';
button_pressed = false; % Sjekker om knappen på NXTen er trykket på

% Definer variabler:
speed = 30; 
Ks = .5;
alpha = 0.8;

% For pid kontroll
Kp= .25;
Kd = 0.001;
Ki = 0.1;

P06_F1_InitializeNXT 
P06_F2_GetFirstMeasurement

while ~JoyMainSwitch && ~button_pressed 
    P06_F3_GetNewMeasurement
    P06_F4_MathCalculations
    P06_F5_CalculateAndSetMotorPower
end

fprintf('Score: %.1f\n',Score(end)) % Skriv ut Score
P06_F6_PlottData
P06_F7_CloseMotorsAndSensors




