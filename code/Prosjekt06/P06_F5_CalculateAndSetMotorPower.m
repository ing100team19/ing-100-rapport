%Ks reduserer farten når roboten svinger
PowerA(k) = speed - Styring(k) - Ks*abs(Styring(k));
PowerB(k) = speed + Styring(k) - Ks*abs(Styring(k));

PowerA(k) = max(min(round(PowerA(k)), 100), -100);
PowerB(k) = max(min(round(PowerB(k)), 100), -100);

if online
    motorA.Power = PowerA(k);
    motorB.Power = PowerB(k);
        
    motorA.SendToNXT();
    motorB.SendToNXT();
else
    pause(0.01) 
end
