function [ character ] = DecodeMorse( morseNumber )
	alpha = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', ... 
	         'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', ...
	         'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
	value = [5, 24, 26, 12, 2, 18, 14, 16, 4, 23, 13, 20, 7, 6, 15, 22, 29, ...
			 10, 8, 3, 9, 17, 11, 25, 27, 28, 47, 39, 35, 33, 32, 48, 56, ...
			 60, 62, 63];
	morse=containers.Map(value, alpha);
	if isKey(morse, morseNumber)
		character=morse(morseNumber);
	else
	    character='?';
	end      
end

