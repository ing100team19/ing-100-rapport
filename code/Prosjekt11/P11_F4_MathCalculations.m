switch valg
    case 0
        disp('Du avbrøt programmet')
        JoyMainSwitch=1;
    case 1 % Tekst til morse (skrift)
        TtM=('');
        tekst=input('Hva ønsker du å oversette til morse?: ','s');
        for i=1:length(tekst)       
        TtM=[TtM,' ',StringToMorse(tekst(i))];     
        end
        disp(TtM)
        JoyMainSwitch=1;       
    case 2 % Tekst til morse (lyd)
        TtM=('');
        tekst=input('Hva ønsker du å oversette til morse?: ','s');
        for i=1:length(tekst)             
        TtM=[TtM,' ',StringToMorse(tekst(i))];  
        end
        disp(TtM)
        periode=75;
        for i=1:length(TtM)
            switch TtM(i)
                case '-'
                    % Lag lang lyd
                    NXT_PlayTone(440, periode*3)
                    pause(periode*3/1000)
                    pause(periode/1000)
                case '.'
                    % Lag kort Lyd
                    NXT_PlayTone(440, periode)
                    pause(periode/1000)
                    pause(periode/1000)
                case ' '
                    pause(periode*3/1000)
                case '/'
                    pause(periode*7/1000)
                otherwise
                    pause(periode/1000)
            end
        end 
        JoyMainSwitch=1;  
    case 3 % Morse til tekst(Etter)
        if ~JoyThumbSwitch
            if Lyd(k)>terskel && Lyd(k-1)<terskel % Positiv flanke           
                flaSta(sigNum)=Tid(k);            
                if sigNum>1
                    sigMel(sigNum-1)=flaSta(sigNum)-flaSlu(sigNum-1);
                end             
            elseif Lyd(k)<terskel && Lyd(k-1)>terskel
                flaSlu(sigNum)=Tid(k);
                sigLen(sigNum)=flaSlu(sigNum)-flaSta(sigNum);
                sigNum=sigNum+1;
            end
            if online==0 && k==numel(Lyd)
                JoyThumbSwitch=1; 
            end
        else
            
            peri1=mean(sigLen(sigLen<min(sigLen)*2));          
            peri3=peri1*3;
            peri7=peri1*7;
            for i=1:(sigNum-1)
                if sigLen(i)< peri1*LimP && sigLen(i)>peri1*LimN
                    morBin=morBin*2;
                elseif sigLen(i)< peri3*LimP && sigLen(i)>peri3*LimN
                    morBin=morBin*2+1;
                end
                if i<(sigNum-1)%Det ferre mellomrom enn signal
                    if sigMel(i)< peri3*LimP && sigMel(i)>peri3*LimN
                        morStr(strTel)=DecodeMorse(morBin);                     
                        strTel=strTel+1;
                        morBin=1;
                    elseif sigMel(i)>peri7*LimN
                        morStr(strTel)=DecodeMorse(morBin);
                        morStr(strTel+1)=' ';
                        strTel=strTel+2;
                        morBin=1;
                    end
                end
            end
            morStr(strTel)=DecodeMorse(morBin);
            disp(morStr);
            JoyMainSwitch=1;         
        end
    case 4 % Morse til tekst(live)
        if ordFerdig==1 % Registrerer når første ord er ferdig          
            peri3=peri1*3;
            peri7=peri1*7;
            for i=1:(sigNum-1)%Loop som oversetter første ord
                if sigLen(i)< peri1*LimP && sigLen(i)>peri1*LimN
                    morBin=morBin*2;
                elseif sigLen(i)< peri3*LimP && sigLen(i)>peri3*LimN
                    morBin=morBin*2+1;
                end
                if i<(sigNum-1)
                    if sigMel(i)< peri3*LimP && sigMel(i)>peri3*LimN
                        morStr(strTel)=DecodeMorse(morBin);                     
                        strTel=strTel+1;
                        morBin=1;
                    elseif sigMel(i)>peri7*LimN
                        morStr(strTel)=DecodeMorse(morBin);
                        morStr(strTel+1)=' ';
                        strTel=strTel+2;
                        morBin=1;
                    end
                end
            end
            morStr(strTel)=DecodeMorse(morBin);
            morStr(strTel+1)=' ';
            strTel=strTel+2;
            conTrans=1; % Første ord er ferdig, oversettelse kan fortsettes
            morBin=1;
            disp(morStr)
        end
        if conTrans==1 % Etter første ord er mottatt
            ordFerdig=0;
            if ordStartet~=1 % Første gang hoppes denne over fordi va har flankestart fra før
                if Lyd(k)>terskel && Lyd(k-1)<terskel % Positiv flanke
                    flaSta(sigNum)=Tid(k);
                    sigMel(sigNum-1)=flaSta(sigNum)-flaSlu(sigNum-1);                   
                    if sigMel(sigNum-1)>peri7*LimN
                        morStr(strTel)=DecodeMorse(morBin);
                        morBin=1;
                        morStr(strTel+1)=' ';
                        strTel=strTel+2;
                    elseif sigMel(sigNum-1)<peri3*LimP && sigMel(sigNum-1)>peri3*LimN
                        morStr(strTel)=DecodeMorse(morBin);
                        morBin=1;
                        strTel=strTel+1;
                    end
                    disp(morStr);
                elseif Lyd(k)<terskel && Lyd(k-1)>terskel %Negativ flanke
                    flaSlu(sigNum)=Tid(k);
                    sigLen(sigNum)=flaSlu(sigNum)-flaSta(sigNum);   
                    if sigLen(sigNum)<peri1*LimP && sigLen(sigNum)>peri1*LimN
                       morBin=morBin*2;
                    elseif sigLen(sigNum)<peri3*LimP && sigLen(sigNum)>peri3*LimN
                       morBin=morBin*2+1;
                    end
                    sigNum=sigNum+1;
                    
                elseif Tid(k)-flaSlu>peri1*20
                    morStr(strTel)=DecodeMorse(morBin);
                    disp(morStr);
                    JoyMainSwitch=1;
                end
            elseif Lyd(k)<terskel && Lyd(k-1)>terskel %Negativ flanke
                   flaSlu(sigNum)=Tid(k);
                   sigLen(sigNum)=flaSlu(sigNum)-flaSta(sigNum);
                   if sigLen(sigNum)<peri1*LimP && sigLen(sigNum)>peri1*LimN
                       morBin=morBin*2;
                   elseif sigLen(sigNum)<peri3*LimP && sigLen(sigNum)>peri3*LimN
                       morBin=morBin*2+1;
                   end
                   sigNum=sigNum+1;
                   ordStartet=0;
            end
        else % Første ord. Brukes for å definere periode lengde
            if Lyd(k)>terskel && Lyd(k-1)<terskel % Positiv flanke   
                flaSta(sigNum)=Tid(k);
                if sigNum>1
                    sigMel(sigNum-1)=flaSta(sigNum)-flaSlu(sigNum-1);     
                end
                if peri1~=0;
                    if sigMel(sigNum-1)>peri1*7*LimN;
                        ordFerdig=1;
                    end
                end
            elseif Lyd(k)<terskel && Lyd(k-1)>terskel % Negativ flanke
                flaSlu(sigNum)=Tid(k);
                sigLen(sigNum)=flaSlu(sigNum)-flaSta(sigNum);
                sigNum=sigNum+1;
                peri1=mean(sigLen(sigLen<min(sigLen)*2));
            end           
        end
    otherwise       
        JoyMainSwitch=1;      
end

