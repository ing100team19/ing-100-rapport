function [Secant] = Differentiation(sensorVector, timeStep)
	% sensorVector: En vektor med de to siste målingene
	% timeStep: Tiden mellom de to målingene

    Secant = (sensorVector(2) - sensorVector(1))/timeStep;
end