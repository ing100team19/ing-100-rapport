﻿function[FilteredValue] = IIR_filter(OldFilteredValue, CurrentSensorValue, Parameter)
	% OldFilteredValue er forrige filtrerte verdien
	% Paramenter er et tall mellom 0 og 1
	% CurrentSensorValue er den nyeste verdien som skal filtreres
    
    FilteredValue = Parameter*CurrentSensorValue + (1-Parameter)*OldFilteredValue;
end