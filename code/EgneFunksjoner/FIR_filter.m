function [FilteredValue] = FIR_filter(SensorVector, m)
	% m er et heltall nummer av verdier som er med i resultatet til filteret
	% SensorVector er en vektor med sensorverdier
   
    % Dersom m<length(SensorVector) brukes alle verdiene i SensorVector
    m = min(length(SensorVector), m);
    FilteredValue =sum(SensorVector(end+1-m:end))/m;
end