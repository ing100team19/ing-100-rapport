function [newIntegratedValue] = Integration( oldIntegratedValue, signal, timeStep, Function)
	% oldIntegratedValue: Den integrerte verdien så langt.
	% signal: 
	% 	For Euler Forward: 	Den nyeste verdien til signalet som skal integreres.
	%	For Trapes:			Vektor med de to nyeste verdien til signalet som skal integreres.
	% timeStep: Tiden mellom sist måling og denne.
	% Function: Enten 'euler' eller 'trapes', avhengig a hvilken integrasjonsmetode er ønsket

	if strcmp(Function,'trapes') || strcmp(Function,'Trapes')
	    if isvector(signal) && length(signal) == 2
	        newIntegratedValue = oldIntegratedValue + timeStep*(signal(1) + signal(2))/2;
	    else
	        disp('Error using trapezoid integration: Sensor input must be vector of length 2')
	    end
	elseif  strcmp(Function,'euler') || strcmp(Function,'eulerforward') ...
	        || strcmp(Function,'Euler') || strcmp(Function,'Eulerforward')
	    if isscalar(signal)
	        newIntegratedValue = oldIntegratedValue + signal*timeStep;
	    else
	        disp('Error using rectangle integration: Sensor input must be scalar')
	    end
	else
	        disp('Error using integration function: Undefined function type')
	end
end