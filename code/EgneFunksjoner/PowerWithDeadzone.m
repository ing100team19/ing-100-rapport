function [Power] = PowerWithDeadzone(AxisValue,Factor,Limit)
	% AxisValue: innlest verdi fra akse på joystick, f.eks JoyForover
	% Factor: Verdi for å få riktig powerforhold
	% Limit: Grenseverdi for deadzone, f.eks. 2

	if  abs(AxisValue*Factor)>Limit	
		Power = max(min(AxisValue*Factor,100),-100);
		if Power>0
		   Power = round(Power - Limit^2/(abs(AxisValue*Factor)));
		else
		   Power = round(Power + Limit^2/(abs(AxisValue*Factor)));
		end
	else
		Power=0;
	end
end
