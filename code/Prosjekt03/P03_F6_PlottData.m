figure(fig1)
subplot(2,1,1)
plot(Tid(1:k),Lys(1:k)); 
xlabel('Tid [sek]')
title('Lys')

subplot(2,1,2);
plot(Tid(1:k-1),LysDerivert(1:k-1));
xlabel('Tid [sek]');
title('LysDerivert');

drawnow