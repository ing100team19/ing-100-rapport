faktor = 100/2^15/2;  % konvertering fra 2^15 til 100%
faktorcomp=2;

PowerA(k)=PowerWithDeadzone(JoyForover(k), faktor, 5);
PowerB(k)=PowerWithDeadzone(JoyForover(k), faktor, 5);

PowerA(k)=max(min(round(PowerA(k) + PowerWithDeadzone(JoyRotate(k),faktor/faktorcomp,2)),100),-100);
PowerB(k)=max(min(round(PowerB(k) - PowerWithDeadzone(JoyRotate(k),faktor/faktorcomp,2)),100),-100);

if online
    motorA.Power = PowerA(k);
    motorB.Power = PowerB(k);
    
    motorA.SendToNXT();
    motorB.SendToNXT();
else
    pause(0.01) 
end
